<?php
namespace Blogpost\controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Blogpost\factory\PdoConnection;
use Blogpost\model\Post;
use Ramsey\Uuid\Nonstandard\Uuid;
use Blogpost\factory\PostFactory;


class routeGetPost
{
    private string $id;
    private array $data;

    public function __construct(private Container $container)
    {
        
    }

    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
        $inputs=json_decode($request->getBody()->getContents(),true,512,JSON_THROW_ON_ERROR);
        $pdo=new PdoConnection();
        $con=$pdo($this->container);

        $this->id = $inputs['id'];
        $getPost= new PostFactory($con);

        foreach($getPost->readPost($this->id) as $row)
        {
            $data[]=array(
                'id'        =>$row['id'],
                'title'     =>$row['title'],
                'content'   =>$row['content'],
                'slug'      =>$row['slug'],
                'thumbnail' =>$row['thumbnail'],
                'author'    =>$row['author'],
                'posted_at' =>$row['posted_at']
            );
        }

        return new JsonResponse($data[0]);
    } 
    
}
?>