<?php
namespace Blogpost\controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Blogpost\factory\PdoConnection;
use Blogpost\model\Post;
use Ramsey\Uuid\Nonstandard\Uuid;
use Blogpost\factory\PostFactory;
use Ramsey\Uuid\UuidInterface;

class routeEditPost
{ 

    private UuidInterface $id;

    public function __construct(private Container $container)
    {
        
    }

    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
            $inputs=json_decode($request->getBody()->getContents(),true,512,JSON_THROW_ON_ERROR);
            $pdo=new PdoConnection();
            $con=$pdo($this->container);
            $this->id = Uuid::fromString($inputs['id']);

            $post = new Post($this->id,$inputs['title'],$inputs['content'],$inputs['slug'],$inputs['author'],$inputs['posted_at'],$inputs['thumbnail'],
            $inputs['categories']);
            $addPost= new PostFactory($con);
            $addPost->editPost($post);
            
            return new JsonResponse('Successfully Updated..');
     
    } 
    
}
?>