<?php
namespace Blogpost\controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Blogpost\factory\PdoConnection;
use Ramsey\Uuid\Nonstandard\Uuid;
use Blogpost\factory\CategoryFactory;


class routeGetCategory
{
    private string $id;

    public function __construct(private Container $container)
    {
        
    }

    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
        $inputs=json_decode($request->getBody()->getContents(),true,512,JSON_THROW_ON_ERROR);
        $pdo=new PdoConnection();
        $con=$pdo($this->container);

        $this->id = $inputs['id'];
        $getCategory= new CategoryFactory($con);

        foreach($getCategory->readCategory($this->id) as $row)
        {
            $data[]=array(
                'id'            =>$row['id'],
                'name'          =>$row['name'],
                'description'   =>$row['description']
            );
        }

        return new JsonResponse($data[0]);
    } 
    
}
?>