<?php
namespace Blogpost\controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Blogpost\factory\PdoConnection;
use Blogpost\model\Post;
use Ramsey\Uuid\Nonstandard\Uuid;
use Blogpost\factory\PostFactory;
use Blogpost\model\Category;
use Ramsey\Uuid\UuidInterface;
use Blogpost\factory\CategoryFactory;

class routeEditCategory
{ 

    private UuidInterface $id;

    public function __construct(private Container $container)
    {
        
    }

    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
            $inputs=json_decode($request->getBody()->getContents(),true,512,JSON_THROW_ON_ERROR);
            $pdo=new PdoConnection();
            $con=$pdo($this->container);
            $this->id = Uuid::fromString($inputs['id']);

            $category       = new Category($this->id,$inputs['name'],$inputs['description']);
            $editCategory   = new CategoryFactory($con);
            $editCategory->editCategory($category );
            
            return new JsonResponse('Successfully Updated..');
     
    } 
    
}
?>