<?php
namespace Blogpost\controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Blogpost\factory\PdoConnection;
use Ramsey\Uuid\Nonstandard\Uuid;
use Blogpost\model\Category;
use Blogpost\factory\CategoryFactory;

class routeAddCategory
{ 
    private array $inputs ;

    public function __construct(private Container $container)
    {
        
    }

    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
            $inputs=json_decode($request->getBody()->getContents(),true,512,JSON_THROW_ON_ERROR);
            $pdo=new PdoConnection();
            $con=$pdo($this->container);

            $catogory       = new Category(Uuid::uuid4(),$inputs['name'],$inputs['description']);
            $addCatogory    = new CategoryFactory($con);
            $addCatogory->addCategory($catogory);
            
            return new JsonResponse('New Category Added...');
     
    } 
    
}
?>