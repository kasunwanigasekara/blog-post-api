<?php
namespace Blogpost\controller;

use DI\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Blogpost\factory\PdoConnection;
use Blogpost\model\Post;
use Ramsey\Uuid\Nonstandard\Uuid;
use Blogpost\factory\PostFactory;

class routeAddPost
{ 
    private array $inputs ;

    public function __construct(private Container $container)
    {
        
    }

    public function __invoke(Request $request, Response $response, $args): JsonResponse
    {
            $inputs=json_decode($request->getBody()->getContents(),true,512,JSON_THROW_ON_ERROR);
            $pdo=new PdoConnection();
            $con=$pdo($this->container);

            $post = new Post(Uuid::uuid4(),$inputs['title'],$inputs['content'],$inputs['content'],$inputs['author'],$inputs['posted_at'],$inputs['thumbnail'],
            $inputs['categories']);
            $addPost= new PostFactory($con);
            $addPost->addPost($post);
            
            return new JsonResponse('New Post Added...');
     
    } 
    
}
?>