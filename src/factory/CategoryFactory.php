<?php
namespace Blogpost\factory;

use PDO;
use Blogpost\model\Category;
use Exception;

class CategoryFactory
{
    public function __construct(private PDO $pdo)
    {
        
    }


    public function addCategory(Category $category): void
    {
        $id             =$category->id();
        $name           =$category->name();
        $description    =$category->description();

        
        try{
        $this->pdo->beginTransaction();       
    
        $stm=$this->pdo->prepare('insert into db_blog_post.categories(id,name,description) VALUES(:id,:name,:description)');
        $stm->bindParam(':id', $id);
        $stm->bindParam(':name', $name);
        $stm->bindParam(':description', $description);
        $stm->execute();

        $this->pdo->commit();

        }

         catch(Exception $e)
         {
             $this->pdo->rollBack();
             echo "\n Error insert of category - ", $e->getMessage();
         }

    }

    public function readCategory(string $id): array
    {

        try{     
    
            $stm=$this->pdo->prepare('SELECT a.id,a.name,a.description from db_blog_post.categories a WHERE a.id=:id');
            $stm->bindParam(':id', $id);
            $stm->execute();
            $result = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $result;

        }

            catch(Exception $e)
            {
                echo "\n Error insert of category - ", $e->getMessage();
            }

    }

    public function deleteCategory(string $id): void
    {

        try{
        $this->pdo->beginTransaction();       
    
        $stm=$this->pdo->prepare('DELETE FROM db_blog_post.categories WHERE id=:id');
        $stm->bindParam(':id', $id);
        $stm->execute();
        $this->pdo->commit();

        }

         catch(Exception $e)
         {
             $this->pdo->rollBack();
             echo "\n Error insert of category - ", $e->getMessage();
         }


    }

    public function editCategory(Category $category): void
    {

        $id             =$category->id();
        $name           =$category->name();
        $description    =$category->description();

        try{
        $this->pdo->beginTransaction();       
    
        $stm=$this->pdo->prepare('update db_blog_post.categories SET name=:name,description=:description WHERE id=:id');
        $stm->bindParam(':id', $id);
        $stm->bindParam(':name', $name);
        $stm->bindParam(':description', $description);
        $stm->execute();
        $this->pdo->commit();

        }

         catch(Exception $e)
         {
             $this->pdo->rollBack();
             echo "\n Error Update category - ", $e->getMessage();
         }


    }

}




?>