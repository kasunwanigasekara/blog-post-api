<?php
namespace Blogpost\factory;

use PDO;
use Blogpost\model\post;
use Exception;

class PostFactory
{
    public function __construct(private PDO $pdo)
    {
        
    }


    public function addPost(post $post): void
    {
        $id         =$post->id();
        $title      =$post->title();
        $content    =$post->content();
        $slug       =$post->slug();
        $thumbnail  =$post->thumbnail();
        $author     =$post->author();
        $posted_at  =$post->posted_at();
        $categories =$post->categories();

        
        try{
        $this->pdo->beginTransaction();       
    
        $stm=$this->pdo->prepare('insert into db_blog_post.posts(id,title,content,slug,thumbnail,author,posted_at) VALUES(:id,:title,:content,:slug,:thumbnail,:author,:posted_at)');
        $stm->bindParam(':id', $id);
        $stm->bindParam(':title', $title);
        $stm->bindParam(':content', $content);
        $stm->bindParam(':slug', $slug);
        $stm->bindParam(':thumbnail', $thumbnail);
        $stm->bindParam(':author', $author);
        $stm->bindParam(':posted_at', $posted_at);
        $stm->execute();


        foreach($categories as $cat)
        {
            $stm=$this->pdo->prepare('insert into db_blog_post.posts_categories(id_post,id_category) VALUES(:id,:cat)');
            $stm->bindParam(':id', $id);
            $stm->bindParam(':cat', $cat['id']);
            $stm->execute();  

        }


        $this->pdo->commit();

        }

         catch(Exception $e)
         {
             $this->pdo->rollBack();
             echo "\n Error insert of post - ", $e->getMessage();
         }

    }

    public function readPost(string $id): array
    {

        try{     
    
            $stm=$this->pdo->prepare('SELECT a.id,a.title,a.content,a.slug,a.thumbnail,a.author,a.posted_at FROM db_blog_post.posts a WHERE a.id=:id');
            $stm->bindParam(':id', $id);
            $stm->execute();
            $result = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $result;

        }

            catch(Exception $e)
            {
                echo "\n Error insert of post - ", $e->getMessage();
            }

    }

    public function deletePost(string $id): void
    {

        try{
        $this->pdo->beginTransaction();       
    
        $stm=$this->pdo->prepare('DELETE FROM db_blog_post.posts WHERE id=:id');
        $stm->bindParam(':id', $id);
        $stm->execute();
        $this->pdo->commit();

        }

         catch(Exception $e)
         {
             $this->pdo->rollBack();
             echo "\n Error insert of post - ", $e->getMessage();
         }


    }

    public function editPost(post $post): void
    {

        $id         =$post->id();
        $title      =$post->title();
        $content    =$post->content();
        $slug       =$post->slug();
        $thumbnail  =$post->thumbnail();
        $author     =$post->author();
        $posted_at  =$post->posted_at();

        try{
        $this->pdo->beginTransaction();       
    
        $stm=$this->pdo->prepare('update db_blog_post.posts set title=:title,content=:content,slug=:slug,thumbnail=:thumbnail,author=:author,posted_at=:posted_at 
        WHERE id=:id');
        $stm->bindParam(':id', $id);
        $stm->bindParam(':title', $title);
        $stm->bindParam(':content', $content);
        $stm->bindParam(':slug', $slug);
        $stm->bindParam(':thumbnail', $thumbnail);
        $stm->bindParam(':author', $author);
        $stm->bindParam(':posted_at', $posted_at);
        $stm->execute();
        $this->pdo->commit();

        }

         catch(Exception $e)
         {
             $this->pdo->rollBack();
             echo "\n Error Update of post - ", $e->getMessage();
         }


    }

}




?>