<?php

namespace Blogpost\model;

use Ramsey\Uuid\UuidInterface;
use Cocur\Slugify\Slugify;

class Post{

   // private string $thumbnail='http://localhost:8889/thumbnails/like_thumb.png';

    public function __construct( 
        private UuidInterface $id,
        private string $title,
        private string $content,
        private string $slug,
        private string $author,
        private string $posted_at,
        private string $thumbnail,
        private array $categories)
         
    {
       
    }


    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function slug(): string
    {
        $slugify = new Slugify();
        $this->slug=$slugify->slugify($this->slug, "_"); 
        return $this->slug;
    }


    public function thumbnail(): string
    {
        return $this->thumbnail;
    }

    public function author(): string
    {
        return $this->author;
    }

    public function posted_at(): string
    {
        $this->posted_at = strtotime($this->posted_at);
        $this->posted_at =date('Y-m-d H:i:s',$this->posted_at);
        return $this->posted_at;
    }

    public function categories(): array
    {
        return $this->categories;
    }



}
?>

