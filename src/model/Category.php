<?php

namespace Blogpost\model;

use Ramsey\Uuid\UuidInterface;

class Category{

    public function __construct( 
        private UuidInterface $id,
        private string $name,
        private string $description)     
    {}


    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function description(): string
    {
        return $this->description;
    }



}
?>

