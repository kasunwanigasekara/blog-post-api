<?php
 require_once __DIR__.'/../boot.php';

use Blogpost\factory\PdoConnection;
use DI\Container; 

 $container =new Container();

 $container->set('settings',function() {
    return [
            'db'=>[
            'host'=>$_ENV['DB_HOST'],
            'dbname'=>$_ENV['DB_NAME'],
            'user'=>$_ENV['DB_USER'],
            'pass'=>$_ENV['DB_PASS'],]
           ];
        });


        $container->set('db', static function ($c) {
            $object = new PdoConnection();
            return $object($c);
});

//$container->set(UrlRepository::class, static fn($c) => (new UrlRepositoryFactory())($c));

return $container;


?>