<?php

use Blogpost\controller\routeAddPost;
use Blogpost\controller\routeGetPost;
use Blogpost\controller\routeEditPost;
use Blogpost\controller\routeDeletePost;

use Blogpost\controller\routeAddCategory;
use Blogpost\controller\routeGetCategory;
use Blogpost\controller\routeEditCategory;
use Blogpost\controller\routeDeleteCategory;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require_once __DIR__.'/../boot.php';

// Create Container using PHP-DI
$container = require_once __DIR__.'/../config/config.php';

// Set container to create App with on AppFactory
AppFactory::setContainer($container);
$app = AppFactory::create();

$app->post('/post/add',new routeAddPost($container));
$app->get('/post/read',new routeGetPost($container));
$app->post('/post/delete',new routeDeletePost($container));
$app->post('/post/edit',new routeEditPost($container));

$app->post('/category/add',new routeAddCategory($container));
$app->get('/category/read',new routeGetCategory($container));
$app->post('/category/delete',new routeDeleteCategory($container));
 $app->post('/category/edit',new routeEditCategory($container));

$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$app->run();

?>